<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta property="og:type" content="article" />
    <title>MonetaGo</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://monetago.com/wp-content/uploads/2016/10/cropped-M-favicon-512-32x32.png" sizes="32x32">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/select2.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<header>
    <nav class="navbar navbar-toggleable-md fixed-top">
        <div class="full-width">
            <div class="row">
                <nav class="navbar container navbar-toggleable-md navbar-light fixed-top">
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar_content" aria-controls="navbar_content" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="img/MonetaGo%20logo.png" alt="logo"></a>
                    <div class="collapse navbar-collapse" id="navbar_content">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="dashboard.php">Dashboard <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="start-new.php">Start New</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="auth.php">Authentication</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="suppliers.php">Suppliers</a>
                            </li>
                            <li class="float-right p-relative">
                                <a href="#" data-toggle="collapse" data-target="#menubar" class="title">Amit &nbsp<img src="img/profile-icon.png" alt="Profile"> </a>
                                <ul class="collapse" id="menubar">
                                    <li class="nav-item">
                                        <a class="nav-link" href="login.php">Profile <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Sign Out <span class="sr-only">(current)</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
        </div>
    </nav>
</header>
