<?php @include 'header.php'; ?>
<style>html{min-height: 100%;}</style>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <div class="border-box">
                    <a href="#">
                        <img src="img/dash-pro.png" alt="Request" title="Request">
                        <h2>15</h2>
                        <p>Requested</p>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="border-box">
                    <a href="#">
                        <img src="img/dash-msg.png" alt="Messege" title="Messege">
                        <h2>6</h2>
                        <p>To review</p>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="border-box">
                    <a href="#">
                        <img src="img/dash-auth.png" alt="Authentication" title="Authentication">
                        <h2>5</h2>
                        <p>Authenticate</p>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="border-box">
                    <a href="#">
                        <img src="img/dash-cancel.png" alt="Failed" title="Failed">
                        <h2>9</h2>
                        <p>Failed</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12">
                <h4>Latest Activity</h4>
                <table class="table table-hover mt-3">
                    <thead>
                    <tr>
                        <th>Supplier</th>
                        <th>GSTIN</th>
                        <th>Request ID</th>
                        <th>Invoices</th>
                        <th>Status</th>
                        <th>Registered</th>
                        <th>Confirmed</th>
                        <th>Matched</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Venosis Pvt Ltd</td>
                        <td>22175A496B531Z5</td>
                        <td>636AE06175B5</td>
                        <td>6</td>
                        <td>Pending supplier action</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    <tr>
                        <td>Venosis Pvt Ltd</td>
                        <td>22175A496B531Z5</td>
                        <td>636AE06175B5</td>
                        <td>6</td>
                        <td>Pending supplier action</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    <tr>
                        <td>Venosis Pvt Ltd</td>
                        <td>22175A496B531Z5</td>
                        <td>636AE06175B5</td>
                        <td>6</td>
                        <td>Pending supplier action</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    <tr>
                        <td>Venosis Pvt Ltd</td>
                        <td>22175A496B531Z5</td>
                        <td>636AE06175B5</td>
                        <td>6</td>
                        <td>Pending supplier action</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    <tr>
                        <td>Venosis Pvt Ltd</td>
                        <td>22175A496B531Z5</td>
                        <td>636AE06175B5</td>
                        <td>6</td>
                        <td>Pending supplier action</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</section>


<?php @include 'footer.php' ?>
