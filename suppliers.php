<?php @include 'header.php' ?>
<style>html{min-height: 100%;}</style>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="suppliers_list">
                    <h4 class=" mb-4">Suppliers List<a href="add-suppliers.php" class="float-right btn btn-primary">+ Supplier</a></h4>
                    <table class="table table-hover text-left">
                        <thead>
                        <tr>
                            <th>Organization Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Contact</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ASD Pvt Ltd</td>
                            <td>example@google.co.in</td>
                            <td>201 Main Street Chicago, Illinois</td>
                            <td><a href="tel:+114-210452">+114-210452</a></td>
                            <td><a href="add-suppliers.php"><i class="fas fa-pencil-alt"></i></a><a href="#" data-toggle="modal" data-target="#deleteConfirm"><i class="far fa-trash-alt"></i></a> </td>
                        </tr>
                        <tr>
                            <td>ASD Pvt Ltd</td>
                            <td>example@google.co.in</td>
                            <td>201 Main Street Chicago, Illinois</td>
                            <td><a href="tel:+114-210452">+114-210452</a></td>
                            <td><a href="add-suppliers.php"><i class="fas fa-pencil-alt"></i></a><a href="#" data-toggle="modal" data-target="#deleteConfirm"><i class="far fa-trash-alt"></i></a> </td>
                        </tr>
                        <tr>
                            <td>ASD Pvt Ltd</td>
                            <td>example@google.co.in</td>
                            <td>201 Main Street Chicago, Illinois</td>
                            <td><a href="tel:+114-210452">+114-210452</a></td>
                            <td><a href="add-suppliers.php"><i class="fas fa-pencil-alt"></i></a><a href="#" data-toggle="modal" data-target="#deleteConfirm"><i class="far fa-trash-alt"></i></a> </td>
                        </tr>
                        <tr>
                            <td>ASD Pvt Ltd</td>
                            <td>example@google.co.in</td>
                            <td>201 Main Street Chicago, Illinois</td>
                            <td><a href="tel:+114-210452">+114-210452</a></td>
                            <td><a href="add-suppliers.php"><i class="fas fa-pencil-alt"></i></a><a href="#" data-toggle="modal" data-target="#deleteConfirm"><i class="far fa-trash-alt"></i></a> </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" tabindex="-1" id="deleteConfirm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center">
                    <h5 class="mb-4">Are you sure want to delete?</h5>
                    <div class="col-md-8 m-auto">
                        <a href="#" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</a>
                        <a href="#" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Yes</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php @include 'footer.php' ?>
