<?php @include 'header.php'; ?>
<style>html{min-height: 100%;}</style>
<section>
    <div class="container">
        <div class="row">
            <div class="fix-width-middle mt-4 mb-4">
                <h5 class="text-center">Invoice Authentication Service</h5>
                <p class="text-center">FCI Pvt Ltd has requested invoice verification. Please follow the instructions on the following screens to complete invoice verification. Invoice verification must be completed before financing can be processed.</p>
                <div class="table">
                    <h6>Details</h6>
                    <table class="table text-left table-hover">
                        <tr>
                            <td>Requestor:</td>
                            <td><label>FCI Pvt Ltd</label></td>
                        </tr>
                        <tr>
                            <td>Supplier:</td>
                            <td><label>Venosis Pvt Ltd</label></td>
                        </tr>
                        <tr>
                            <td>Request Date:</td>
                            <td><label>03/12/2017</label></td>
                        </tr>
                        <tr>
                            <td>Number of Invoices:</td>
                            <td><label>6</label></td>
                        </tr>
                        <tr>
                            <td>Total Invoice Value:</td>
                            <td><label>INR 302,21,100</label></td>
                        </tr>
                    </table>
                </div>
                <div class="float-left">
                    <a href="#">View Invoices</a>
                </div>
                <div class="float-right">
                    <a href="invoice_step2.php" class="btn btn-primary">Next<span class="pl-2"><img src="img/right_arrow.png"> </span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php'; ?>
