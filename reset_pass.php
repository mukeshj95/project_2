<?php @include 'header.php' ?>
    <style>html{min-height: 100%;}</style>
    <section class="bg-light full-h">
        <div class="container">
            <div class="row">
                <div class="fix-width-middle start text-center">
                    <h3>Invoice Authentication Service</h3>
                    <form action="dashboard.php" method="POST">
                        <div class="login-box">
                            <h4>Reset Your Password</h4>
                            <div class="form-group d-flex">
                                <label for="newPassword" class="sr-only"></label>
                                <input type="password" class="form-control" id="newPassword" placeholder="New Password" title="New Password" required>
                            </div>
                            <div class="form-group d-flex">
                                <label for="confirmPassword" class="sr-only"></label>
                                <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password" title="Confirm Password" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" value="Sign In">Reset Password</button>
                            </div>
                            <a  class="text-center" href="login.php">Login</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php @include 'footer.php' ?>
