<?php ?>

<footer>
    <div class="container">
        <div class="row">

            <ul><li>Copyright © 2018 MonetaGo Inc &nbsp All rights reserved</li></ul>
            <ul class="ml-auto">
                <li>
                    <a href="#">Privacy Policy</a>
                </li>
                <li>
                    <a href="#">Terms & Conditions</a>
                </li>
            </ul>
        </div>
    </div>
</footer>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
