<?php @include 'header.php'; ?>

<style>html{min-height: 100%;}</style>
<section>
    <div class="container">
        <div class="row">
            <div class="fix-width-middle">
                <h3 class="text-center mb-5">Start a new authentication request to supplier</h3>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="orgName">Organization Name</label>
                        <select id="orgName" class="form-control">
                            <option value="opt1" selected>Option 1</option>
                            <option>Option 1</option>
                            <option>Option 1</option>
                            <option>Option 1</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="registeredEmail">Registered Email</label>
                        <input type="text" class="form-control" id="registeredEmail" placeholder="Email Id">
                    </div>
                    <div class="form-group import_group">
                        <div class="float-left">
                            <a href="#" data-toggle="modal" data-target="#upload_file">
                                <label for="invoices"><img src="img/button_upload.png">Import invoices</label>
                            </a>
                        </div>
                        <div class="float-right check_box">
                            <input type="checkbox" id="checkInvoices">
                            <label for="checkInvoices">Supplier to provide invoice details</label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <a href="#" class="manualEntry">Manual Invoice Entry</a>
                    </div>
                    <div class="form-group">
                        <div class="float-right">
                            <a href="#" data-toggle="modal" data-target="#submit_confirm" class="btn btn-primary">Submit</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>


            </div>
        </div>
    </div>
</section>
<hr>
<div class="container">
    <div class="row">
        <div class="fix-width-middle">
            <h4 class="text-center mb-4">Invoices to Authenticate</h4>
            <table class="table table-hover text-left">
                <thead>
                <tr>
                    <th>Invoice Number</th>
                    <th>Buyer GSTIN</th>
                    <th>Invoice Date</th>
                    <th>Invoice Value</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>A/00002125442</td>
                    <td>19AAAWWB047AZZ</td>
                    <td>27/12/2017</td>
                    <td>INR 12,50,00</td>
                </tr>
                <tr>
                    <td>A/00002125442</td>
                    <td>19AAVGFGGB047AZZ</td>
                    <td>27/12/2017</td>
                    <td>INR 12,50,00</td>
                </tr>
                <tr>
                    <td>A/00002125442</td>
                    <td>19AAVGFGGB047AZZ</td>
                    <td>27/12/2017</td>
                    <td>INR 12,50,00</td>
                </tr>
                <tr>
                    <td>A/00002125442</td>
                    <td>19AAVFGHBN47AZZ</td>
                    <td>27/12/2017</td>
                    <td>INR 12,50,00</td>
                </tr>
                </tbody>
            </table>
            <hr>
            <div class="col-md-12">
                <p>6/6 Invoices imported successfully <label class="float-right"><a href="#">Remove All &nbsp<img src="img/right_arrow.png"></a></label> </p>
            </div>
            <br />
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" id="upload_file" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Import Invoices</h4>&nbsp;
            </div>
            <div class="modal-body">
                <h5 class="mb-2">Invoices must be uploaded as csv with the same column definitions as GSTN b2b uploads</h5>
                <p><a href="#" >Download .CSV Template</a></p>
                <form action="">
                    <div class="upload_box">
                        <label for="upload_item"><img src="img/button_upload.png">
                            <span>Choose a file</span></label>
                        <input type="file" class="form-control" id="upload_item">
                        <div class="form-group">
                            <div class="float-right mt-3">
                                <input type="submit" class="btn btn-primary" value="Submit">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="submit_confirm" role="dialog" aria-labelledby="myModalSubmit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Confirmation</h4>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close"><img src="img/close_icon.png">
                </a>
            </div>
            <div class="modal-body">
                <h5>Your request has been sent to Venosis pvt ltd</h5>
                <div>
                    <img src="img/tick.png">
                </div>
                <p>Number of Invoices</p>
                <h5>6</h5>
                <p>Total invoices value</p>
                <p><b>INR 302,20,000</b></p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" id="searchOrg" role="dialog" aria-labelledby="searchOrg" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" class="close float-right" data-dismiss="modal" aria-label="Close"><img src="img/close_icon.png">
                </a>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label></label>
                </div>
            </div>
        </div>
    </div>
</div>
<?php @include 'footer.php'?>

