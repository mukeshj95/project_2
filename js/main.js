$(document).on('click', function (e){
    /* bootstrap collapse js adds "in" class to your collapsible element*/
    var menu_opened = $('#menubar').hasClass('show');

    if(!$(e.target).closest('#menubar').length &&
        !$(e.target).is('#menubar') &&
        menu_opened === true){
        $('#menubar').collapse('toggle');
    }

});
$('[data-plugin="matchHeight"]').each(function() {
    var $this = $(this),
        options = $.extend(true, {}, null, $this.data()),
        matchSelector = $this.data('matchSelector');

    if (matchSelector) {
        $this.find(matchSelector).matchHeight(options);
    } else {
        $this.children().matchHeight(options);
    }

});
$('select.form-control').select2();



$(".navbar-nav li a").on("click", function() {
    $(".navbar-nav li a").removeClass("active");
    $(this).addClass("active");
});

$(function(){
    $(".fold-table tr.view1").on("click", function(){
        $(this).toggleClass('view, open');
        $(".fold1").toggleClass("open").next(".fold1").toggleClass("open");
    });
});
$(function(){
    $(".fold-table tr.view2").on("click", function(){
        $(this).toggleClass('view, open');
        $(".fold2").toggleClass("open").next(".fold2").toggleClass("open");
    });
});
$(document).ready(function () {
    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("navbar-collapse in");
        if (_opened === true && !clickover.hasClass("navbar-toggle")) {
            $("button.navbar-toggle").click();
        }
    });
});
