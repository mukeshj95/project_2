<?php @include 'header.php' ?>
<style>html{min-height: 100%;}</style>
<section class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="mb-3">Invoice Authentication</h4>
                <table class="table fold-table">
                    <thead>
                    <tr>
                        <th>Supplier</th>
                        <th>Invoice Date</th>
                        <th>Invoice No.</th>
                        <th>Invoice Value</th>
                        <th>Status</th>
                        <th>Registered</th>
                        <th>Confirmed</th>
                        <th>Matched</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="view view1">
                        <td>Venosis Pvt Ltd</td>
                        <td>21/01/2018</td>
                        <td>636AE06175B5</td>
                        <td>INR 12,50,000</td>
                        <td>Fully Authenticated</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    <tr class="fold1 fold">
                        <td colspan="8">
                            <div class="fold-content">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Request Date</th>
                                        <th>Supplier GSTIN</th>
                                        <th>Buyer GSTIN </th>
                                        <th>Processed as of</th>
                                        <th>Verifying Contact</th>
                                        <th>Approval</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>636AE06175B5</td>
                                        <td>01/01/2018</td>
                                        <td>2215ARF152Z</td>
                                        <td>ASE2356744FG</td>
                                        <td>01/01/2018 13:25:15</td>
                                        <td>Ram Kumar</td>
                                        <td>Not Reviewed</td>
                                        <td><select class="">
                                                <option>Mark as Accepted </option>
                                                <option>Mark as Rejected</option>
                                                <option>Re-request Authentication</option>
                                            </select>
                                            <a href="#" value="submit"></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr class="view2 view">
                        <td>Venosis Pvt Ltd</td>
                        <td>21/01/2018</td>
                        <td>636AE06175B5</td>
                        <td>INR 12,50,000</td>
                        <td>Fully Authenticated</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    <tr class="fold2 fold">
                        <td colspan="8">
                            <div class="fold-content">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Request Date</th>
                                        <th>Supplier GSTIN</th>
                                        <th>Buyer GSTIN </th>
                                        <th>Processed as of</th>
                                        <th>Verifying Contact</th>
                                        <th>Approval</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>636AE06175B5</td>
                                        <td>01/01/2018</td>
                                        <td>2215ARF152Z</td>
                                        <td>ASE2356744FG</td>
                                        <td>01/01/2018 13:25:15</td>
                                        <td>Ram Kumar</td>
                                        <td>Not Reviewed</td>
                                        <td><select class="">
                                                <option>Mark as Accepted </option>
                                                <option>Mark as Rejected</option>
                                                <option>Re-request Authentication</option>
                                            </select>
                                            <a href="#" value="submit"></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Venosis Pvt Ltd</td>
                        <td>21/01/2018</td>
                        <td>636AE06175B5</td>
                        <td>INR 12,50,000</td>
                        <td>Fully Authenticated</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    <tr>
                        <td>Venosis Pvt Ltd</td>
                        <td>21/01/2018</td>
                        <td>636AE06175B5</td>
                        <td>INR 12,50,000</td>
                        <td>Fully Authenticated</td>
                        <td><span class="dot-success"></span></td>
                        <td><span class="dot-danger"></span></td>
                        <td><span class="dot-success"></span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</section>
<?php @include 'footer.php'?>
