<?php @include 'header.php' ?>
    <style>html{min-height: 100%;}</style>
    <section class="full-h">
        <div class="container">
            <div class="row">
                <div class="fix-width-middle start text-center">
                    <h3>Invoice Authentication Service</h3>
                    <form action="dashboard.php" method="POST">
                        <div class="login-box">
                            <h4>Login</h4>
                            <div class="form-group d-flex">
                                <label for="email" class="sr-only"></label>
                                <input type="text" class="form-control" id="email" placeholder="Your Email" required>
                            </div>
                            <div class="form-group d-flex">
                                <label for="password" class="sr-only"></label>
                                <input type="password" class="form-control" id="password" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-wide" value="Sign In">Sign In</button>
                            </div>
                            <a class="text-center" href="reset_pass.php">Forgot Password</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php @include 'footer.php' ?>