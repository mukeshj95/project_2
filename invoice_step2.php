<?php @include 'header.php'; ?>
<style>html{min-height: 100%;}</style>
<section>
    <div class="container">
        <div class="row">
            <div class="fix-width-middle mt-4 mb-4">
                <h5 class="text-center">Invoice Authentication Service</h5>
                <p class="text-center">In order to verify the authenticity of the invoices submitted. you must authorize access to your GSTN filings.</p>
                <p class="text-center">Please read the terms and conditions before continuing.</p>
                <div class="table">
                    <h6 class="text-center">Terms and Conditions</h6>
                    <div>
                        <ul><li>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis
                            </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis
                            </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis
                            </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="check_box">
                    <input type="checkbox" id="checkTerms">
                    <label for="checkTerms">I have read the terms and conditions.</label>
                </div>
                <div class="check_box">
                    <input type="checkbox" id="checkAuth">
                    <label for="checkAuth">I authorize MonetaGo to share my information with FCI Pvt Ltd.</label>
                </div>
                <div class="float-right">
                    <a href="invoice_step3.php" class="btn btn-primary">Next<span class="pl-2"><img src="img/right_arrow.png"> </span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php'; ?>
