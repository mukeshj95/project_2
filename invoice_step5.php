<?php @include 'header.php'; ?>
<style>html{min-height: 100%;}</style>
<section>
    <div class="container">
        <div class="row">
            <div class="fix-width-middle mt-4 mb-4 text-center">
                <h5 class="text-center">Invoice Authentication Service</h5>
                <p>Thank You</p>
                <p>Authentication Process is complete.</p>
                <p>The result of invoice authentication will automatically be provided back to FCI Pvt Ltd.</p>
                <p>You may now close this page.</p>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php'; ?>
