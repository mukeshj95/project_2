<?php @include 'header.php'; ?>
<style>html{min-height: 100%;}</style>
<section>
    <div class="container">
        <div class="row">
            <div class="fix-width-middle mt-4 mb-4">
                <h5 class="text-center">Invoice Authentication Service</h5>
                <p class="text-center">Please enter your GSTN user information and complete the OTP request. Your user Id must be registered for access to Venosis Pvt Ltd filings.</p>
                <form action="invoice_step3.php" method="post">
                    <div class="form-group">
                        <label for="userOTP"></label>
                        <input class="form-control" placeholder="Enter OTP" id="userOTP" title="Enter OTP" type="number">
                    </div>
                    <div class="form-group">
                        <a href="invoice_step5.php" class="btn btn-primary float-right">Next<span class="pl-2"><img src="img/right_arrow.png"></span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php'; ?>
