<?php @include 'header.php'; ?>

<style>html{min-height: 100%;}</style>
<section>
    <div class="container">
        <div class="row">
            <div class="fix-width-middle">
                <h3 class="text-center mb-5">Supplier Details</h3>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="organizationName">Organization Name</label>
                        <input id="organizationName" class="form-control" name="OrganizationName" title="Organization Name" placeholder="Organization Name">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="registeredEmail">Registered Email</label>
                            <input type="email" class="form-control" id="registeredEmail" placeholder="Email Id">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact">Contact Details</label>
                            <input type="number" class="form-control" id="contact" placeholder="Contact Details">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address1">Registered Address</label>
                        <input type="text" class="form-control" id="address1" placeholder="Address 1">
                    </div>
                    <div class="form-group">
                        <label for="address2" class="sr-only"></label>
                        <input type="text" class="form-control" id="address2" placeholder="Address 2">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="city" class="sr-only"></label>
                            <input type="text" class="form-control" id="city" placeholder="City">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="zipcode" class="sr-only"></label>
                            <input type="number" class="form-control" id="zipcode" placeholder="Zipcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="float-right">
                            <a href="#" data-toggle="modal" data-target="#submit_confirm" class="btn btn-primary">Submit</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php @include 'footer.php'?>

